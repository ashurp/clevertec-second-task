//
//  UserCellModel.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 11.02.22.
//

import CoreGraphics

struct UserCellModel {

    let number: Int
    let iconNumber: Int
    let iconHeight: CGFloat = 60

    init(number: Int, iconNumber: Int) {
        self.number = number
        self.iconNumber = iconNumber
    }
}
