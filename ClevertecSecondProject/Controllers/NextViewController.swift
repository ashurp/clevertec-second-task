//
//  NextViewController.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 8.02.22.
//

import UIKit

final class NextViewController: UIViewController {

    var model: UserCellModel?
    lazy var icon = UIImageView()
    lazy var titleLabel = UILabel()
    lazy var descriptionLabel = UILabel()
    lazy var iconHeight: CGFloat? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        view.backgroundColor = .systemBackground
        setupLayout()
    }
}

extension NextViewController {
    func setupLayout() {
        let mainStackView = UIStackView(axis: .vertical, spacing: 20,
                                        elements: [icon, titleLabel, descriptionLabel])

        icon.layer.masksToBounds = true
        icon.backgroundColor = .lightGray
        icon.tintColor = .black
        icon.contentMode = .center
        icon.setViewLayout(height: iconHeight, width: iconHeight)
        if let iconHeight = iconHeight {
            icon.cornerRadius = iconHeight / 2
        }

        self.view.addSubview(mainStackView)

        mainStackView.setViewLayout(height: 200, width: 200)
        let centerConstraintsArray = centerConstraints(item: mainStackView)
        NSLayoutConstraint.activate(centerConstraintsArray)
    }

    func setupData(with model: UserCellModel) {
        let icons: [String] =  IconName.allCases.map { $0.rawValue }
        icon.image = UIImage(named: icons[model.iconNumber])
        iconHeight = model.iconHeight * 2

        titleLabel.text = StringConstants.title.rawValue.capitalized + " \(model.number)"
        descriptionLabel.text = StringConstants.description.rawValue.capitalized + " \(model.number)"
    }
}
