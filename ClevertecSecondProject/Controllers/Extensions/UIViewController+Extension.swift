//
//  UIViewController+Extension.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 10.02.22.
//

import UIKit

extension UIViewController {

    func centerConstraints(item: UIView) -> [NSLayoutConstraint] {
        let centerVertically = NSLayoutConstraint(item: item,
                                                  attribute: .centerX,
                                                  relatedBy: .equal,
                                                  toItem: view,
                                                  attribute: .centerX,
                                                  multiplier: 1,
                                                  constant: 0)
        let centerHorizontally = NSLayoutConstraint(item: item,
                                                    attribute: .centerY,
                                                    relatedBy: .equal,
                                                    toItem: view,
                                                    attribute: .centerY,
                                                    multiplier: 1,
                                                    constant: 0)

        return [centerVertically, centerHorizontally]
    }
}
