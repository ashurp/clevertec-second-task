//
// UIView+Extension.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 8.02.22.
//

import UIKit

extension UIView {

    // MARK: - View Radius

    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }

    // MARK: - Top Radius

    var topCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            layer.cornerRadius = newValue
        }
    }

    // MARK: - Bottom Radius

    var bottomCornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            layer.cornerRadius = newValue
        }
    }

    // MARK: - Border Width

    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    // MARK: - Border Color

    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
