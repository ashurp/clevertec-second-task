//
//  Enumerators.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 8.02.22.
//

enum IconName: String, CaseIterable {
    case build
    case done
    case favourite
    case fingerprint
    case language
    case lightbulb
    case lock
    case paid
    case visibility
    case work
}

enum StringConstants:String {
    case title
    case description
}
