//
//  ViewController.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 8.02.22.
//

import UIKit

final class MainViewController: UIViewController {

    private let tableView = UITableView(frame: .zero, style: .plain)
    private var cells: [UserCellModel] = []
    private var numberOfRows = 1000
    private var editMode = false

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        populateCellArray()
        setupTableView()

        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(longPressGestureRecognizer:)))
        self.view.addGestureRecognizer(longPressRecognizer)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Setup Table View

    private func setupTableView() {
        tableView.backgroundColor = .clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.dragInteractionEnabled = true
        tableView.showsVerticalScrollIndicator = false
        view.addSubview(tableView)
        tableView.setViewLayout(leading: 0, trailing: 0, top: 0, bottom: 0)
        tableView.register(UserCell.self, forCellReuseIdentifier: UserCell.reuseIdentifier)
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }

    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
            let touchPoint = longPressGestureRecognizer.location(in: self.view)
            if self.tableView.indexPathForRow(at: touchPoint) != nil {
                if self.editMode {
                    tableView.isEditing = true
                } else {
                    tableView.isEditing = false
                }
                self.editMode.toggle()
            }
        }
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UserCell.reuseIdentifier) as? UserCell else {
            fatalError("Could not dequeue cell with identifier: \(UserCell.reuseIdentifier)")
        }
        cell.setupCellData(with: cells[indexPath.row])

        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            numberOfRows -= 1
            cells.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = NextViewController()
        let model = cells[indexPath.row]
        nextVC.setupData(with: model)
        
        navigationController?.pushViewController(nextVC, animated: true)
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let cell = cells[sourceIndexPath.row]
        cells.remove(at: sourceIndexPath.row)
        cells.insert(cell, at: destinationIndexPath.row)
    }

    func populateCellArray() {
        for i in 0 ..< numberOfRows {
            let cellNumber = i + 1
            let iconNumber = i % 10

            let cellModel = UserCellModel(number: cellNumber, iconNumber: iconNumber)
            cells.append(cellModel)
        }
    }
}
