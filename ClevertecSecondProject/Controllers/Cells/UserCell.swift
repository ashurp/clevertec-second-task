//
//  UserCell.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 8.02.22.
//

import UIKit

final class UserCell: UITableViewCell {

    var model: UserCellModel?
    var cellNumber: Int?
    let iconHeight: CGFloat = 60

    lazy var cellIcon = UIImageView()
    lazy var titleLabel = UILabel()
    lazy var descriptionLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        cellIcon.image = nil
        titleLabel.text = ""
        descriptionLabel.text = ""
    }
}

extension UserCell {

    private func setupLayout() {
        let labelStack = UIStackView(axis: .vertical, alignment: .leading, spacing: 12,
                                     elements: [titleLabel, descriptionLabel])
        let mainStackView = UIStackView(spacing: 20, elements: [cellIcon, labelStack])

        titleLabel.textAlignment = .left
        descriptionLabel.textAlignment = .left
        
        cellIcon.layer.masksToBounds = true
        cellIcon.backgroundColor = .lightGray
        cellIcon.tintColor = .black
        cellIcon.contentMode = .center
        cellIcon.setViewLayout(height: iconHeight, width: iconHeight)
        cellIcon.cornerRadius = iconHeight / 2

        self.backgroundColor = .clear
        self.selectionStyle = .none
        self.contentView.isUserInteractionEnabled = false

        self.addSubview(mainStackView)
        mainStackView.setViewLayout(leading: 20, trailing: 20, top: 16, bottom: 16)
    }

    func setupCellData(with model: UserCellModel) {
        let icons: [String] =  IconName.allCases.map { $0.rawValue }
        cellIcon.image = UIImage(named: icons[(model.iconNumber)])

        titleLabel.text = StringConstants.title.rawValue.capitalized + " \(model.number)"
        descriptionLabel.text = StringConstants.description.rawValue.capitalized + " \(model.number)"
    }
}
