//
//  SceneDelegate.swift
//  ClevertecSecondProject
//
//  Created by Alexandra Shurpeleva on 8.02.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        let navigation = UINavigationController()
        let mainVC = MainViewController()
        navigation.viewControllers = [mainVC]
        window?.rootViewController = navigation
        window?.makeKeyAndVisible()
    }
}

